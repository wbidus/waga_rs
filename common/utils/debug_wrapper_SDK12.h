#ifndef DEBUG_WRAPPER_SDK12_H
#define DEBUG_WRAPPER_SDK12_H

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#define LOG(fmt, ...)   NRF_LOG_INFO(fmt, ##__VA_ARGS__);
#define LOG_INIT        NRF_LOG_INIT

#endif //DEBUG_WRAPPER_SDK12_H
