#ifndef HW_PLATFORM_H
#define HW_PLATFORM_H

#if defined (HW_PLATFORM_RECEIVER_V1)
  #include "receiver_v1.h"
#elif defined (HW_PLATFORM_TRANSMITTER_V1)
  #include "transmitter_v1.h"
#else
#error "Board is not defined"
#endif

#endif // HW_PLATFORM_H
