/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#ifndef __MICRO_ESB_H
#define __MICRO_ESB_H

#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"

// Hard coded parameters - change if necessary
#define     UESB_CORE_MAX_PAYLOAD_LENGTH    20

// Configuration parameter definitions

typedef enum {
    UESB_BITRATE_2MBPS     = RADIO_MODE_MODE_Nrf_2Mbit,      /**< 2Mbit radio mode.                                             */
    UESB_BITRATE_1MBPS     = RADIO_MODE_MODE_Nrf_1Mbit,      /**< 1Mbit radio mode.                                             */
    UESB_BITRATE_250KBPS   = RADIO_MODE_MODE_Nrf_250Kbit,    /**< 250Kbit radio mode.                                           */
    UESB_BITRATE_1MBPS_BLE = RADIO_MODE_MODE_Ble_1Mbit       /**< 1Mbit radio mode using Bluetooth Low Energy radio parameters. */
} uesb_bitrate_t;

typedef enum {
    UESB_CRC_16BIT = RADIO_CRCCNF_LEN_Two,
    UESB_CRC_8BIT  = RADIO_CRCCNF_LEN_One,
    UESB_CRC_OFF   = RADIO_CRCCNF_LEN_Disabled
} uesb_crc_t;

typedef enum {
    UESB_TX_POWER_4DBM     = RADIO_TXPOWER_TXPOWER_Pos4dBm,
    UESB_TX_POWER_0DBM     = RADIO_TXPOWER_TXPOWER_0dBm,
    UESB_TX_POWER_NEG4DBM  = RADIO_TXPOWER_TXPOWER_Neg4dBm,
    UESB_TX_POWER_NEG8DBM  = RADIO_TXPOWER_TXPOWER_Neg8dBm,
    UESB_TX_POWER_NEG12DBM = RADIO_TXPOWER_TXPOWER_Neg12dBm,
    UESB_TX_POWER_NEG16DBM = RADIO_TXPOWER_TXPOWER_Neg16dBm,
    UESB_TX_POWER_NEG20DBM = RADIO_TXPOWER_TXPOWER_Neg20dBm,
    UESB_TX_POWER_NEG30DBM = RADIO_TXPOWER_TXPOWER_Neg30dBm
} uesb_tx_power_t;

// Internal state definition
typedef enum {
    UESB_STATE_UNINITIALIZED,
    UESB_STATE_IDLE,
    UESB_STATE_PTX_TX,
} uesb_mainstate_t;

typedef void (*uesb_event_handler_t)(void);

// Main UESB configuration struct, contains all radio parameters
typedef struct
{
    uesb_event_handler_t    event_handler;

    // General RF parameters
    uesb_bitrate_t          bitrate;
    uesb_crc_t              crc;
    uint8_t                 rf_channel;
    uint8_t                 payload_length;
    uint8_t                 rf_addr_length;

    uesb_tx_power_t         tx_output_power;
    uint8_t                 tx_address[5];
    uint8_t                 rx_address_p0[5];
    uint8_t                 rx_address_p1[5];
    uint8_t                 rx_address_p2;
    uint8_t                 rx_address_p3;
    uint8_t                 rx_address_p4;
    uint8_t                 rx_address_p5;
    uint8_t                 rx_address_p6;
    uint8_t                 rx_address_p7;
    uint8_t                 rx_pipes_enabled;

    uint8_t                 radio_irq_priority;
}uesb_config_t;

// Default radio parameters, roughly equal to nRF24L default parameters (except CRC which is set to 16-bit, and protocol set to DPL)
#define UESB_DEFAULT_CONFIG {.event_handler         = 0,                                \
                             .rf_channel            = 2,                                \
                             .payload_length        = UESB_CORE_MAX_PAYLOAD_LENGTH,     \
                             .rf_addr_length        = 5,                                \
                             .bitrate               = UESB_BITRATE_2MBPS,               \
                             .crc                   = UESB_CRC_16BIT,                   \
                             .tx_output_power       = UESB_TX_POWER_0DBM,               \
                             .rx_address_p0         = {0xE7, 0xE7, 0xE7, 0xE7, 0xE7},   \
                             .rx_address_p1         = {0xC2, 0xC2, 0xC2, 0xC2, 0xC2},   \
                             .rx_address_p2         = 0xC3,                             \
                             .rx_address_p3         = 0xC4,                             \
                             .rx_address_p4         = 0xC5,                             \
                             .rx_address_p5         = 0xC6,                             \
                             .rx_address_p6         = 0xC7,                             \
                             .rx_address_p7         = 0xC8,                             \
                             .rx_pipes_enabled      = 0x3F,                             \
                             .radio_irq_priority    = 1}

typedef enum {UESB_ADDRESS_PIPE0, UESB_ADDRESS_PIPE1, UESB_ADDRESS_PIPE2, UESB_ADDRESS_PIPE3, UESB_ADDRESS_PIPE4, UESB_ADDRESS_PIPE5, UESB_ADDRESS_PIPE6, UESB_ADDRESS_PIPE7} uesb_address_type_t;

typedef struct
{
    uint8_t length;
    uint8_t pipe;
    int8_t  rssi;
    uint8_t noack;
    uint8_t data[UESB_CORE_MAX_PAYLOAD_LENGTH];
}uesb_payload_t;

uint32_t uesb_init(uesb_config_t *parameters);

uint32_t uesb_txen(void);

uint32_t uesb_disable(void);

bool     uesb_is_idle(void);

uint32_t uesb_write_tx_payload(uesb_payload_t *payload);

uint32_t uesb_write_tx_payload_noack(uesb_payload_t *payload);

uint32_t uesb_write_ack_payload(uesb_payload_t *payload);

uint32_t uesb_read_rx_payload(uesb_payload_t *payload);

uint32_t uesb_start_tx(void);

uint32_t uesb_start_rx(void);

uint32_t uesb_stop_rx(void);

uint32_t uesb_get_tx_attempts(uint32_t *attempts);

uint32_t uesb_flush_tx(void);

uint32_t uesb_flush_rx(void);

uint32_t uesb_get_clear_interrupts(uint32_t *interrupts);

uint32_t uesb_set_address(uesb_address_type_t address, const uint8_t *data_ptr);

uint32_t uesb_set_rf_channel(uint32_t channel);

uint32_t uesb_set_tx_power(uesb_tx_power_t tx_output_power);

void uesb_set_recv(uint8_t* buffer, uint8_t len);

#endif
