#ifndef __SEG_DISP_DEFS_H
#define __SEG_DISP_DEFS_H

#define DIG0    0xC0
#define DIG1    0xF9
#define DIG2    0xA4
#define DIG3    0xB0
#define DIG4    0x99
#define DIG5    0x92
#define DIG6    0x82
#define DIG7    0xF8
#define DIG8    0x80
#define DIG9    0x90

#define DOT     0x7F
#define MINUS   0xBF

#define POS0    4
#define POS1    2
#define POS2    1
#define POS3    64
#define POS4    32
#define POS5    16

#endif //__SEG_DISP_DEFS_H
