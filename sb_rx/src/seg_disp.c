#include "stdint.h"
#include "stdbool.h"
#include "nrf_gpio.h"
#include "seg_disp.h"
#include "seg_disp_defs.h"
#include "hw_platform.h"
#include "nrf_drv_spi.h"

#define NRF_LOG_MODULE_NAME "SEG"
#include "debug_wrapper_SDK12.h"

#define DISP_MAX_VAL    (99999)

#define SPI_INSTANCE 0

static const uint8_t digit_defs[10] = {DIG0, DIG1, DIG2, DIG3, DIG4, DIG5, DIG6, DIG7, DIG8, DIG9};
static const uint8_t pos_defs[6] = {POS0, POS1, POS2, POS3, POS4, POS5};

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_busy = false;  /**< Flag used to indicate that SPI instance completed the transfer. */

static bool is_err = false; //lost communication indicator
static bool is_neg = false; //is number negative
static int32_t val = 0;

static uint8_t m_tx_buf[2];

static uint8_t get_val_digit(uint8_t digit)
{
    uint32_t temp = (uint32_t)val;
    uint8_t result;
    switch(digit)
    {
        case 0:
            break;
        case 1:
            temp = temp/10;
            break;
        case 2:
            temp = temp/100;
            break;
        case 3:
            temp = temp/1000;
            break;
        case 4:
            temp = temp/10000;
            break;
        case 5:
            return 10;
        default:
            return 0;
    }
    result = temp%10;
    return result;
}

void disp_val_set(int32_t value)
{
    if(value<0)
        is_neg = true;
    else
        is_neg = false;
    
    if(is_neg)
        val = value*(-1);
    else
        val = value;
    
    if(val>DISP_MAX_VAL)
        val = DISP_MAX_VAL;
}

void spi_event_handler(nrf_drv_spi_evt_t const * p_event)
{
    spi_busy = false;
}

void disp_init(void)
{
    nrf_gpio_cfg_output(PIN_DISP_LATCH);
    nrf_gpio_pin_set(PIN_DISP_LATCH);
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.frequency = NRF_DRV_SPI_FREQ_4M;
    spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;
    spi_config.miso_pin = NRF_DRV_SPI_PIN_NOT_USED;
    spi_config.mosi_pin = PIN_DISP_MOSI;
    spi_config.sck_pin  = PIN_DISP_CLK;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler));
}

void disp_error_set(bool set)
{
    is_err = set;
}


void disp_process(void)
{
    uint32_t err_code;
    if(val<0)
        return;
    static uint8_t next_digit = 0;
    uint8_t val_digit = get_val_digit(next_digit);
    if(val_digit<10)
    {
        m_tx_buf[0] = digit_defs[val_digit];
    }
    else
    {
        m_tx_buf[0] = 0xFF;
        if(is_err)
            m_tx_buf[0] &= DOT;
        if(is_neg)
            m_tx_buf[0] &= MINUS;
    }

    m_tx_buf[1] = pos_defs[next_digit];
    
    next_digit++;
    if(next_digit>5)
        next_digit=0;
    
    if(spi_busy)
        return;
    nrf_gpio_pin_clear(PIN_DISP_LATCH);
    spi_busy = true;
    err_code = nrf_drv_spi_transfer(&spi, m_tx_buf, 2, NULL, 0);
    APP_ERROR_CHECK(err_code);
    nrf_gpio_pin_set(PIN_DISP_LATCH);
}
