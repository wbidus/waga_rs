#include "micro_esb.h"
#include "uesb_error_codes.h"
#include "nrf_gpio.h"
#include <string.h>

#include "nrf_delay.h"

static uesb_event_handler_t     m_event_handler;

static uesb_payload_t payload;

static uint8_t* recv_buffer = NULL;
static uint8_t recv_len = 0;

// Function that swaps the bits within each byte in a uint32. Used to convert from nRF24L type addressing to nRF51 type addressing
static uint32_t bytewise_bit_swap(uint32_t inp)
{
    inp = (inp & 0xF0F0F0F0) >> 4 | (inp & 0x0F0F0F0F) << 4;
    inp = (inp & 0xCCCCCCCC) >> 2 | (inp & 0x33333333) << 2;
    return (inp & 0xAAAAAAAA) >> 1 | (inp & 0x55555555) << 1;
}

uint32_t uesb_init(uesb_config_t *parameters)
{
    m_event_handler = parameters->event_handler;
    
    NRF_RADIO->TXPOWER   = parameters->tx_output_power   << RADIO_TXPOWER_TXPOWER_Pos;
    NRF_RADIO->MODE      = parameters->bitrate           << RADIO_MODE_MODE_Pos;
    
    NRF_RADIO->PCNF0 = (0 << RADIO_PCNF0_S0LEN_Pos) | (0    << RADIO_PCNF0_LFLEN_Pos)   | (0 << RADIO_PCNF0_S1LEN_Pos);
    NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Enabled         << RADIO_PCNF1_WHITEEN_Pos) |
                       (RADIO_PCNF1_ENDIAN_Big              << RADIO_PCNF1_ENDIAN_Pos)  |
                       ((parameters->rf_addr_length - 1)    << RADIO_PCNF1_BALEN_Pos)   |
                       (parameters->payload_length          << RADIO_PCNF1_STATLEN_Pos) |
                       (parameters->payload_length          << RADIO_PCNF1_MAXLEN_Pos);
    NRF_RADIO->BASE0   = bytewise_bit_swap(parameters->rx_address_p0[1] << 24 | parameters->rx_address_p0[2]<< 16 | parameters->rx_address_p0[3]<< 8 | parameters->rx_address_p0[4]);
    NRF_RADIO->BASE1   = bytewise_bit_swap(parameters->rx_address_p1[1] << 24 | parameters->rx_address_p1[2]<< 16 | parameters->rx_address_p1[3]<< 8 | parameters->rx_address_p1[4]);
    
    NRF_RADIO->PREFIX0 = bytewise_bit_swap(parameters->rx_address_p3    << 24 | parameters->rx_address_p2   << 16 | parameters->rx_address_p1[0]<< 8 | parameters->rx_address_p0[0]);
    NRF_RADIO->PREFIX1 = bytewise_bit_swap(parameters->rx_address_p7    << 24 | parameters->rx_address_p6   << 16 | parameters->rx_address_p5   << 8 | parameters->rx_address_p4);
    
    NRF_RADIO->TXADDRESS = 0;
    NRF_RADIO->RXADDRESSES = RADIO_RXADDRESSES_ADDR0_Enabled;

    NRF_RADIO->CRCCNF    = parameters->crc               << RADIO_CRCCNF_LEN_Pos;
    NRF_RADIO->CRCPOLY = 0x11021UL;     // CRC poly: x^16+x^12^x^5+1
    NRF_RADIO->CRCINIT = 0xFFFFUL;      // Initial value
    
    NRF_RADIO->TIFS = 0;
    
    NRF_RADIO->PACKETPTR = (uint32_t)payload.data;
    
    NRF_RADIO->FREQUENCY = (parameters->rf_channel)&0x7F;
    if((parameters->rf_channel)&0x80)
        NRF_RADIO->FREQUENCY |= 0x100;
    NRF_RADIO->DATAWHITEIV = (parameters->rf_channel)&0x7F;

    NRF_RADIO->SHORTS      = RADIO_SHORTS_READY_START_Msk | RADIO_SHORTS_END_START_Msk | RADIO_SHORTS_ADDRESS_RSSISTART_Msk | RADIO_SHORTS_DISABLED_RSSISTOP_Msk;
    NRF_RADIO->INTENSET    = RADIO_INTENSET_END_Msk;

    NRF_RADIO->EVENTS_READY = 0;
    NRF_RADIO->TASKS_RXEN  = 1;
    while(!NRF_RADIO->EVENTS_READY);
    NRF_RADIO->EVENTS_READY = 0;
    
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_EnableIRQ(RADIO_IRQn);
    NRF_RADIO->INTENSET    = RADIO_INTENSET_END_Msk;
    
    return UESB_SUCCESS;
}

void uesb_set_recv(uint8_t* buffer, uint8_t len)
{
    recv_buffer = buffer;
    recv_len = len;
}

void RADIO_IRQHandler()
{
    if(NRF_RADIO->EVENTS_END && (NRF_RADIO->INTENSET & RADIO_INTENSET_END_Msk))
    {
        NRF_RADIO->EVENTS_END = 0;
        if(NRF_RADIO->CRCSTATUS != 0 && m_event_handler != 0)
        {
            if(recv_buffer!=NULL)
                memcpy(recv_buffer, payload.data, recv_len);
            m_event_handler();
        }
    }
}
