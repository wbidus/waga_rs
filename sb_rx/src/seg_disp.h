#ifndef __SEG_DISP_H
#define __SEG_DISP_H

#include "stdint.h"
#include "stdbool.h"

void disp_val_set(int32_t val);
void disp_init(void);
void disp_error_set(bool set);
void disp_process(void);


#endif //__SEG_DISP_H
