#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "app_error.h"
#include "app_timer_appsh.h"
#include "app_scheduler.h"
#include "micro_esb.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "seg_disp.h"

#define NRF_LOG_MODULE_NAME "APP"
#include "debug_wrapper_SDK12.h"

#define SCHED_MAX_EVENT_DATA_SIZE       sizeof(app_timer_event_t)
#define SCHED_QUEUE_SIZE                10

#define APP_TIMER_OP_QUEUE_SIZE          4 

#define ERR_CHECK_INTERVAL              100

#define PCOUNTER_INTERVAL               1000

static uint32_t pcounter=0;

APP_TIMER_DEF(m_err_check_timer);
APP_TIMER_DEF(m_pcounter_timer);

#define RX_PAYLOAD_LEN   (sizeof(int32_t))

static int32_t rx_buffer;
static bool received = false;

void HardFault_Handler(void)
{
    #ifndef DEBUG
    wakemngr_get_sleep();
    NVIC_SystemReset();
    #else
    while(true){};
    //NVIC_SystemReset();
    #endif //DEBUG
}

void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    #ifdef ENABLE_DEBUG_LOG_SUPPORT
    LOG("ASRT:%s,%d,e0x%08x\r\n", (uint32_t)((error_info_t*)info)->p_file_name, (uint32_t)((error_info_t*)info)->line_num, (uint32_t)((error_info_t*)info)->err_code);
    #else
    wakemngr_get_sleep();
    sd_nvic_SystemReset();
    #endif
}

static void radio_event_handler(void)
{
    received = true;
    disp_error_set(false);
    disp_val_set(rx_buffer);
    pcounter++;
}

static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

static void err_check_handler(void * p_context)
{
    if(!received)
        disp_error_set(true);
    received = false;
}

static void pcounter_handler(void * p_context)
{
    //disp_val_set(pcounter);
    pcounter = 0;
}

static void timers_init(void)
{
    uint32_t err_code;
    // Initialize timer module.
    APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
    
    err_code = app_timer_create(&m_err_check_timer, APP_TIMER_MODE_REPEATED, err_check_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_create(&m_pcounter_timer, APP_TIMER_MODE_REPEATED, pcounter_handler);
    APP_ERROR_CHECK(err_code);
}

static void timers_start(void)
{
    uint32_t err_code;
    err_code = app_timer_start(m_err_check_timer, APP_TIMER_TICKS(ERR_CHECK_INTERVAL, APP_TIMER_PRESCALER), NULL);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_start(m_pcounter_timer, APP_TIMER_TICKS(PCOUNTER_INTERVAL, APP_TIMER_PRESCALER), NULL);
    APP_ERROR_CHECK(err_code);
}

static void clocks_init(void)
{
    NRF_CLOCK->LFCLKSRC = CLOCK_LFCLKSRC_SRC_RC;
    
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    NRF_CLOCK->TASKS_LFCLKSTART = 1;
        
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);
    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);
}

static void radio_init(void)
{
    uint8_t rx_addr_p0[] = {0x43, 0xBC, 0xCB, 0xD7, 0x8D};
    uint8_t rx_addr_p1[] = {0xBC, 0xDE, 0xF0, 0x12, 0x23};
    uint8_t rx_addr_p2   = 0x66;

    uesb_config_t uesb_config       = UESB_DEFAULT_CONFIG;
    uesb_config.rf_channel          = 100;
    uesb_config.crc                 = UESB_CRC_16BIT;
    uesb_config.bitrate = UESB_BITRATE_250KBPS;

    uesb_config.event_handler       = radio_event_handler;
    uesb_config.rf_addr_length      = 3;
    uesb_config.tx_output_power     = UESB_TX_POWER_4DBM;

    memcpy(uesb_config.rx_address_p0, rx_addr_p0, uesb_config.rf_addr_length);
    memcpy(uesb_config.rx_address_p1, rx_addr_p1, uesb_config.rf_addr_length);
    uesb_config.rx_address_p2 = rx_addr_p2;

    uesb_config.payload_length = RX_PAYLOAD_LEN;
    uesb_set_recv((uint8_t*)&rx_buffer, sizeof(uint32_t));
    uesb_init(&uesb_config);
}

static void gpio_init(void)
{

}


/**
 * @brief Function for main application entry.
 */
int main(void)
{
    nrf_delay_ms(1000);
    uint32_t err_code;
    clocks_init();
    err_code = LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    LOG("Started!\r\n");
    scheduler_init();
    gpio_init();
    timers_init();
    timers_start();
    radio_init();
    disp_init();
    while(1)
    {
        //__WFI();
        app_sched_execute();
        disp_process();
    }
}

/** @} */
