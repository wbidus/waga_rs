#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "app_error.h"
#include "app_timer_appsh.h"
#include "app_scheduler.h"
#include "micro_esb.h"
#include "nrf_gpio.h"
#include "data_parser.h"

#define NRF_LOG_MODULE_NAME "APP"
#include "debug_wrapper_SDK12.h"

#define SCHED_MAX_EVENT_DATA_SIZE       sizeof(app_timer_event_t)
#define SCHED_QUEUE_SIZE                10

#define APP_TIMER_OP_QUEUE_SIZE          4 

#define TX_TRIG_INTERVAL            10 //tx trig interval (msecs)
APP_TIMER_DEF(m_tx_trig_timer);


#define TX_PAYLOAD_LEN   (sizeof(int32_t))
static uesb_payload_t tx_payload;


void HardFault_Handler(void)
{
    #ifndef DEBUG
    wakemngr_get_sleep();
    NVIC_SystemReset();
    #else
    while(true){};
    //NVIC_SystemReset();
    #endif //DEBUG
}

void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    #ifdef ENABLE_DEBUG_LOG_SUPPORT
    LOG("ASRT:%s,%d,e0x%08x\r\n", (uint32_t)((error_info_t*)info)->p_file_name, (uint32_t)((error_info_t*)info)->line_num, (uint32_t)((error_info_t*)info)->err_code);
    #else
    wakemngr_get_sleep();
    sd_nvic_SystemReset();
    #endif
}

static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

static void tx_trig_handler(void * p_context)
{
    int32_t testdata = parser_val_get();
    memcpy((uint8_t*)&tx_payload.data[0],(uint8_t*)&testdata,sizeof(int32_t));
    uesb_write_tx_payload_noack(&tx_payload);
}

static void timers_init(void)
{
    uint32_t err_code;
    // Initialize timer module.
    APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
    
    err_code = app_timer_create(&m_tx_trig_timer, APP_TIMER_MODE_REPEATED, tx_trig_handler);
    APP_ERROR_CHECK(err_code);
}

static void timers_start(void)
{
    uint32_t err_code;
    err_code = app_timer_start(m_tx_trig_timer, APP_TIMER_TICKS(TX_TRIG_INTERVAL, APP_TIMER_PRESCALER), NULL);
    APP_ERROR_CHECK(err_code);
}

static void clocks_init(void)
{
    NRF_CLOCK->LFCLKSRC = CLOCK_LFCLKSRC_SRC_RC;
    
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    NRF_CLOCK->TASKS_LFCLKSTART = 1;
        
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);
    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);
}


static void radio_init(void)
{
    uint8_t rx_addr_p0[] = {0x43, 0xBC, 0xCB, 0xD7, 0x8D};
    uint8_t rx_addr_p1[] = {0xBC, 0xDE, 0xF0, 0x12, 0x23};
    uint8_t rx_addr_p2   = 0x66;

    uesb_config_t uesb_config       = UESB_DEFAULT_CONFIG;
    uesb_config.rf_channel          = 100;
    uesb_config.crc                 = UESB_CRC_16BIT;
    uesb_config.bitrate = UESB_BITRATE_250KBPS;

    uesb_config.event_handler       = NULL;
    uesb_config.rf_addr_length      = 3;
    uesb_config.tx_output_power     = UESB_TX_POWER_4DBM;

    memcpy(uesb_config.rx_address_p0, rx_addr_p0, uesb_config.rf_addr_length);
    memcpy(uesb_config.rx_address_p1, rx_addr_p1, uesb_config.rf_addr_length);
    uesb_config.rx_address_p2 = rx_addr_p2;

    uesb_config.payload_length = TX_PAYLOAD_LEN;
    tx_payload.length = TX_PAYLOAD_LEN;
    tx_payload.noack    = true;
    tx_payload.pipe    = 0;

    uesb_init(&uesb_config, true);
}

static void gpio_init(void)
{
    nrf_gpio_cfg_output(22);
}

/**
 * @brief Function for main application entry.
 */
int main(void)
{
    uint32_t err_code;
    clocks_init();
    err_code = LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    LOG("Started!\r\n");
    gpio_init();
    scheduler_init();
    timers_init();
    timers_start();
    radio_init();
    parser_init();
    while(1)
    {
        __WFI();
        parser_process();
        app_sched_execute();
    }
}

/** @} */
