#include "stdint.h"
#include "stdbool.h"
#include "uart_fifo.h"
#include "nrf_uart.h"
#include "hw_platform.h"
#include "app_util_platform.h"

#define NRF_LOG_MODULE_NAME "UART_FIFO"
#include "debug_wrapper_SDK12.h"

static uint8_t rx_head, rx_tail, rx_cnt;
static uint8_t tx_head, tx_tail, tx_cnt;

static uint8_t rx_fifo[UART_FIFO_SIZE];
static uint8_t tx_fifo[UART_FIFO_SIZE];

static bool fifo_rx_add(uint8_t byte)
{
    if(rx_cnt==UART_FIFO_SIZE)
        return false;
    rx_fifo[rx_head] = byte;
    rx_head++;
    rx_head&=UART_FIFO_SIZE_OVRFLW_MASK;
    rx_cnt++;
    return true;
}

static bool fifo_tx_add(uint8_t byte)
{
    if(tx_cnt==UART_FIFO_SIZE)
        return false;
    tx_fifo[tx_head] = byte;
    tx_head++;
    tx_head&=UART_FIFO_SIZE_OVRFLW_MASK;
    tx_cnt++;
    return true;
}

bool uart_fifo_rx_get(uint8_t* byte)
{
    if (!rx_cnt)
        return false;
    *byte = rx_fifo[rx_tail];
    rx_tail++;
    rx_tail&=UART_FIFO_SIZE_OVRFLW_MASK;
    rx_cnt--;
    return true;
}

static bool fifo_tx_get(uint8_t* byte)
{
    if (!tx_cnt)
        return false;
    *byte = tx_fifo[tx_tail];
    tx_tail++;
    tx_tail&=UART_FIFO_SIZE_OVRFLW_MASK;
    tx_cnt--;
    return true;
}

bool uart_fifo_rx_has_data(void)
{
    return (bool)rx_cnt;
}

static void rx_irqhandler(void)
{
    uint8_t byte;
    byte = nrf_uart_rxd_get(NRF_UART0);
    fifo_rx_add(byte);
}

static void tx_irqhandler(void)
{
    uint8_t byte;
    if(tx_cnt)
    {
        fifo_tx_get(&byte);
        nrf_uart_txd_set(NRF_UART0, byte);
    }
}

static void err_irqhandler(void)
{
    LOG("Uart error!\r\n");
}
static void uart_init(void)
{
    nrf_uart_txrx_pins_set(NRF_UART0, PIN_TX, PIN_RX);
    nrf_uart_baudrate_set(NRF_UART0, NRF_UART_BAUDRATE_9600);
    nrf_uart_hwfc_pins_disconnect(NRF_UART0);
    nrf_uart_configure(NRF_UART0, NRF_UART_PARITY_EXCLUDED, NRF_UART_HWFC_DISABLED);
    nrf_uart_int_enable(NRF_UART0, UART_INTENSET_ERROR_Msk | UART_INTENSET_TXDRDY_Msk | UART_INTENSET_RXDRDY_Msk);
    NVIC_SetPriority(UART0_IRQn, APP_IRQ_PRIORITY_LOW);
    NVIC_ClearPendingIRQ(UART0_IRQn);
    nrf_uart_enable(NRF_UART0);
    NVIC_EnableIRQ(UART0_IRQn);
    nrf_uart_task_trigger(NRF_UART0, NRF_UART_TASK_STARTTX);
    nrf_uart_task_trigger(NRF_UART0, NRF_UART_TASK_STARTRX);

}

void UART0_IRQHandler(void)
{
    if(nrf_uart_event_check(NRF_UART0, NRF_UART_EVENT_TXDRDY))
    {
        nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_TXDRDY);
        tx_irqhandler();
    }
    if(nrf_uart_event_check(NRF_UART0, NRF_UART_EVENT_RXDRDY))
    {
        nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_RXDRDY);
        rx_irqhandler();
    }
    if(nrf_uart_event_check(NRF_UART0, NRF_UART_EVENT_ERROR))
    {
        nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_ERROR);
        err_irqhandler();
    }
}

void uart_fifo_init(void)
{
    rx_head=0;
    rx_tail=0;
    rx_cnt=0;
    
    tx_head=0;
    tx_tail=0;
    tx_cnt=0;
    uart_init();
}

void uart_fifo_send(uint8_t* buffer, uint8_t len)
{
    uint8_t byte;
    for(uint8_t i=0; i<len; i++)
    {
        fifo_tx_add(buffer[i]);
    }
    if(tx_cnt)
    {
        fifo_tx_get(&byte);
        nrf_uart_txd_set(NRF_UART0, byte);
    }
        
}
