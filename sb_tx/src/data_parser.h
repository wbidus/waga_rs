#ifndef DATA_PARSER_H
#define DATA_PARSER_H

#include "stdint.h"

void parser_init(void);
void parser_process(void);
int32_t parser_val_get(void);

#endif //DATA_PARSER_H
