#include "nrf_uart.h"
#include "hw_platform.h"
#include "stdint.h"
#include "string.h"
#include "uart_fifo.h"
#include "data_parser.h"

#define NRF_LOG_MODULE_NAME "DATA_PARSER"
#include "debug_wrapper_SDK12.h"

#define BUFFER_SIZE (8)
#define BUFFER_MASK (BUFFER_SIZE - 1)

static int32_t val;
static uint8_t buffer[BUFFER_SIZE];
static uint8_t pivot;
static bool negative;

static void reset_parser(void)
{
    memset(buffer, NULL, sizeof(buffer));
    pivot = 0;
    negative = false;
}

static void value_latch(void)
{
    int32_t value = 0;
    uint16_t multiplier = 1;
    if(pivot>5)
    {
        LOG("Received value is too damn high!\r\n");
        return;
    }
    while(pivot>0)
    {
        pivot--;
        value += (buffer[pivot]-'0')*multiplier;
        multiplier *= 10;
    }
    if(negative)
        value*=(-1);
    val = value;
    LOG("Received value: %d\r\n", val);
}

void parser_init(void)
{
    reset_parser();
    uart_fifo_init();
}


void parser_process(void)
{
    uint8_t recv;
    while(uart_fifo_rx_has_data())
    {
        uart_fifo_rx_get(&recv);
        if((recv>='0') && (recv<='9'))
        {
            if(pivot<BUFFER_SIZE)
            {
                buffer[pivot] = recv;
                pivot++;
            }
        }
        else if(recv=='-')
        {
            negative = true;
        }
        else if(recv=='\r')
        {
            value_latch();
            reset_parser();
        }
    }
    
}

int32_t parser_val_get(void)
{
    return val;
}

