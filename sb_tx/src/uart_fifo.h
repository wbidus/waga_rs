#ifndef UART_FIFO_H
#define UART_FIFO_H
#include "stdint.h"
#include "stdbool.h"

#define UART_FIFO_SIZE   128
#define UART_FIFO_SIZE_OVRFLW_MASK UART_FIFO_SIZE-1

bool uart_fifo_rx_get(uint8_t* byte);
bool uart_fifo_rx_has_data(void);
void uart_fifo_init(void);
void uart_fifo_send(uint8_t* buffer, uint8_t len);

#endif //UART_FIFO_H
